package edu.xunta.agrasar.buscaminas.modelo;

public class Factoria {

	private static Factoria INSTANCE;

	public static Factoria getInstance() {
		if (INSTANCE == null) {
			createInstance();
		}
		return INSTANCE;
	}

	private static synchronized void createInstance() {
		INSTANCE = new Factoria();
	}

	public Buscaminas crearBuscaminas() {
		 return new MiBuscaminas();
	}

}
