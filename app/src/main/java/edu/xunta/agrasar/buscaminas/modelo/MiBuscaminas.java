package edu.xunta.agrasar.buscaminas.modelo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class MiBuscaminas implements Buscaminas {
	private int altura;
	private int anchura;
	private int numTotalMinas;
	private int numMinasRestantes = 0;
	private int numCeldasRestantes = 0;
	private Date comienzoJuego;
	private ArrayList<int[]> coordsMinas = new ArrayList<int[]>();
	public Celda[][] mapa = null;

	@Override
	public Buscaminas init(NivelJuego pNivel) {
		// Diferenciamos el nivel experto para que en la vista se vea correctamente cambiando alto por ancho
		try {
			this.init(pNivel.get_ancho(), pNivel.get_alto(), pNivel.get_numMinas());
		} catch (ReglaBuscaminasException e) {
			// Estan definidas por nosotros, no debería dar problemas
			e.printStackTrace();
		}
		return this;
	}

	@Override
	public Buscaminas init(int pAncho, int pAlto, int pNumMinas) throws ReglaBuscaminasException {
		this.altura = pAlto;
		this.anchura = pAncho;
		this.numMinasRestantes = this.numTotalMinas = pNumMinas;
		if (compruebaMapa()) {
			generarMapa();
		}
		return this;
	}

	@Override
	public Buscaminas init(int pAncho, int pAlto, int[][] pCoordsMinas) throws ReglaBuscaminasException {
		for (int x = 0; x < pCoordsMinas.length; x++) {
			if (pCoordsMinas[x][0] >= pAlto || pCoordsMinas[x][0] < 0 || pCoordsMinas[x][1] >= pAncho || pCoordsMinas[x][1] < 0) {
				throw new ReglaBuscaminasException("Minas colocadas fuera de rango");
			} else {
				this.coordsMinas.add(pCoordsMinas[x]);
			}
		}
		
		this.init(pAncho, pAlto, pCoordsMinas.length);
		
		return this;
	}

	private boolean compruebaMapa() throws ReglaBuscaminasException {
		if (this.numTotalMinas <= 0) {
			throw new ReglaBuscaminasException("Sin minas que colocar");
		}
		if ((this.anchura * this.altura) <= this.numTotalMinas) {
			throw new ReglaBuscaminasException("Explotaste por exceso de minas!!!!!!");
		}
		if (this.anchura < 2 && this.altura < 2) {
			throw new ReglaBuscaminasException("Pocas celdas!!!!!!");
		}
		return true;
	}

	private void generarMapa() {
		this.mapa = new Celda[this.altura][this.anchura];
		for (int x = 0; x < this.altura; x++) {
			for (int y = 0; y < this.anchura; y++) {
				this.mapa[x][y] = new Celda();
			}
		}

		generarMinas();
		ponerValorMinas();
	}

	private void generarMinas() {
		// Si no se pasan coordenadas de minas las generamos

		if (this.coordsMinas.size() == 0) {
			ArrayList<Integer> aY = new ArrayList<>();
			HashMap<Integer, ArrayList<Integer>> map = new HashMap<>();

			for (int x = 0; x < this.altura; x++) {
				aY.add(x);
				map.put(x, new ArrayList<Integer>());
				
				for (int y = 0; y < this.anchura; y++) {
					map.get(x).add(y);
				}
			}

			do {
				int i_alt = ((int) (Math.random() * (aY.size())));
				int alt = aY.get(i_alt);
				int i_anc = (int) (Math.random() * (map.get(alt).size()));
				int anc = map.get(alt).get(i_anc);
				map.get(alt).remove(i_anc);
				int[] a = { alt, anc };
				this.coordsMinas.add(a);
				try {
					if (map.get(alt).size() == 0) {
						map.remove(alt);
						aY.remove(i_alt);
					}
				} catch (Exception e) {
					System.out.println("EX");
				}
			} while (this.numTotalMinas != this.coordsMinas.size());
		}
		// Asignamos minas a las celdas
		for (int[] row : this.coordsMinas) {
			this.mapa[row[0]][row[1]].setMina(true);
		}

	}

	private void generarMinasLOW() {
		//Mas lento anque parezca que non
		// Si no se pasan coordenadas de minas las generamos
		if (this.coordsMinas.size() == 0) {
			ArrayList<int[]> coords = new ArrayList<>();
			for (int x = 0; x < this.altura; x++) {
				for (int y = 0; y < this.anchura; y++) {
					coords.add(new int[] {x,y});
				}
			}
			do {
				int alea = (int) (Math.random() * (coords.size()));
				this.coordsMinas.add(coords.get(alea));
				coords.remove(alea);
			} while (this.numTotalMinas != this.coordsMinas.size());
		}
		// Asignamos minas a las celdas
		for (int[] row : this.coordsMinas) {
			this.mapa[row[0]][row[1]].setMina(true);
		}

	}

	private void ponerValorMinas() {
		for (int x = 0; x < this.altura; x++) {
			for (int y = 0; y < this.anchura; y++) {
				this.mapa[x][y].setValor(cuentaMinas(x, y));
			}
		}
	}

	@Override
	public boolean hayMima(int i, int j) {
		return this.mapa[i][j].isMina();
	}

	@Override
	public boolean estaDescubierta(int i, int j) {
		if (this.mapa[i][j].getEstado() == Celda.estados[3]) {
			return true;
		}
		return false;
	}

	@Override
	public boolean terminoElJuego() {
		if (numCeldasRestantes + numTotalMinas == this.altura * this.anchura) {
			return true;
		}
		return false;
	}

	@Override
	public Buscaminas descubrir(int x, int y) throws ReglaBuscaminasException {
		if (this.mapa[x][y].isMina()) {
			this.mapa[x][y].setEstado(Celda.estados[3]);
		} else if (this.mapa[x][y].getValor() != 0) {
			this.mapa[x][y].setEstado(Celda.estados[3]);
			this.numCeldasRestantes++;
		} else {
			this.mapa[x][y].setEstado(Celda.estados[3]);
			this.numCeldasRestantes++;
			addCeldas(x, y);
		}
		return this;
	}

	private void addCeldas(int x, int y) throws ReglaBuscaminasException {
		int a = x - 1;
		if (a >= 0 && a < this.altura) {
			int b = y - 1;
			if (b >= 0 && b < this.anchura && this.mapa[a][b].getEstado() != Celda.estados[3]) {
				descubrir(a, b);
			}
			b++;
			if (b >= 0 && b < this.anchura && this.mapa[a][b].getEstado() != Celda.estados[3]) {
				descubrir(a, b);
			}
			b++;
			if (b >= 0 && b < this.anchura && this.mapa[a][b].getEstado() != Celda.estados[3]) {
				descubrir(a, b);
			}
		}

		int c = x + 1;
		if (c >= 0 && c < this.altura) {
			int d = y - 1;
			if (d >= 0 && d < this.anchura && this.mapa[c][d].getEstado() != Celda.estados[3]) {
				descubrir(c, d);
			}
			d++;
			if (d >= 0 && d < this.anchura && this.mapa[c][d].getEstado() != Celda.estados[3]) {
				descubrir(c, d);
			}
			d++;
			if (d >= 0 && d < this.anchura && this.mapa[c][d].getEstado() != Celda.estados[3]) {
				descubrir(c, d);
			}
		}

		int e = y - 1;
		if (e >= 0 && e < this.anchura && this.mapa[x][e].getEstado() != Celda.estados[3]) {
			descubrir(x, e);
		}

		int f = y + 1;
		if (f >= 0 && f < this.anchura && this.mapa[x][f].getEstado() != Celda.estados[3]) {
			descubrir(x, f);
		}
	}

	private int cuentaMinas(int x, int y) {
		int count = 0;
		int a = x - 1;

		if (a >= 0 && a < this.altura) {
			int b = y - 1;
			if (b >= 0 && b < this.anchura && this.mapa[a][b].getEstado() != 3 && this.mapa[a][b].isMina()) {
				count++;
			}
			b++;
			if (b >= 0 && b < this.anchura && this.mapa[a][b].getEstado() != 3 && this.mapa[a][b].isMina()) {
				count++;
			}
			b++;
			if (b >= 0 && b < this.anchura && this.mapa[a][b].getEstado() != 3 && this.mapa[a][b].isMina()) {
				count++;
			}
		}

		int c = x + 1;

		if (c >= 0 && c < this.altura) {
			int d = y - 1;

			if (d >= 0 && d < this.anchura && this.mapa[c][d].getEstado() != 3 && this.mapa[c][d].isMina()) {
				count++;
			}
			d++;
			if (d >= 0 && d < this.anchura && this.mapa[c][d].getEstado() != 3 && this.mapa[c][d].isMina()) {
				count++;
			}
			d++;
			if (d >= 0 && d < this.anchura && this.mapa[c][d].getEstado() != 3 && this.mapa[c][d].isMina()) {
				count++;
			}
		}

		int e = y - 1;
		if (e >= 0 && e < this.anchura && this.mapa[x][e].getEstado() != 3 && this.mapa[x][e].isMina()) {
			count++;
		}

		int f = y + 1;
		if (f >= 0 && f < this.anchura && this.mapa[x][f].getEstado() != 3 && this.mapa[x][f].isMina()) {
			count++;
		}
		return count;
	}

	@Override
	public Buscaminas marcarMina(int x, int y) throws ReglaBuscaminasException {
		this.mapa[x][y].setEstado(Celda.estados[2]);
		this.numMinasRestantes--;

		return this;
	}

	@Override
	public Buscaminas marcarDuda(int x, int y) throws ReglaBuscaminasException {
		this.mapa[x][y].setEstado(Celda.estados[4]);
		return this;
	}

	@Override
	public Buscaminas desmarcar(int x, int y) throws ReglaBuscaminasException {
		this.mapa[x][y].setEstado(Celda.estados[0]);
		this.numMinasRestantes++;
		return this;
	}

	public int[][] getCoordsMinas() {
		int[][] a = new int[this.coordsMinas.size()][2];
		for (int x = 0; x < this.coordsMinas.size(); x++) {
			int[] b = this.coordsMinas.get(x);
			a[x] = new int[] { b[0], b[1] };

		}

		return a;
	}

	@Override
	public int getNumMinasProximas(int x, int y) throws ReglaBuscaminasException {
		return this.mapa[x][y].getValor();
	}

	@Override
	public int getAltura() {
		return this.altura;
	}

	@Override
	public int getAnchura() {
		return this.anchura;
	}

	@Override
	public int getNumTotalMinas() {
		return this.numTotalMinas;
	}

	@Override
	public int getNumMinasRestantes() {
		return this.numMinasRestantes;
	}

	@Override
	public Date getComienzoJuego() {
		return this.comienzoJuego;
	}

}
