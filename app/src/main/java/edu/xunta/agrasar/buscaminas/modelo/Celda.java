package edu.xunta.agrasar.buscaminas.modelo;

public class Celda {

	/*
	 * 0 => Agua 1 => Boom!!! 2 => Marcada 3 => Descubierta 4 => Duda
	 */
	public final static int estados[] = { 0, 1, 2, 3, 4 };
	private boolean mina;
	private int estado;
	private int valor;

	public Celda() {
		this.mina = false;
		this.valor = 0;
		this.estado = estados[0];
	}


	public boolean isMina() {
		return mina;
	}

	public void setMina(boolean mina) {
		this.mina = mina;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}

}
