package edu.xunta.agrasar.buscaminas.modelo;

public enum NivelJuego {
	Principiante(8, 8, 10), Estandar(16, 16, 40), Experto(16, 30, 99);

	private final int _ancho;
	private final int _alto;
	private final int _numMinas;

	public int get_ancho() {
		return _ancho;
	}

	public int get_alto() {
		return _alto;
	}

	public int get_numMinas() {
		return _numMinas;
	}

	NivelJuego(int pAncho, int pAlto, int pNumMinas) {
		_ancho = pAncho;
		_alto = pAlto;
		_numMinas = pNumMinas;
	}

}
