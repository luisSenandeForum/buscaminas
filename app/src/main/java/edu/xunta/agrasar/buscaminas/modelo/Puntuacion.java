package edu.xunta.agrasar.buscaminas.modelo;

import java.util.Date;

public class Puntuacion implements Comparable<Puntuacion>{
	private int puntos;
	private String nombre;
	private String dificultad;
	private Date fecha;
	
	public Puntuacion(int pts, String dificultad) {
		this.puntos = pts;
		this.nombre = "";
		this.fecha = new Date();
		this.dificultad = dificultad;
		
	}

	public int getPuntos() {
		return puntos;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDificultad() {
		return dificultad;
	}

	public Date getFecha() {
		return fecha;
	}

	@Override
	public int compareTo(Puntuacion o) {
		 if (getPuntos() < o.getPuntos()) {
             return -1;
         }
         if (getPuntos() > o.getPuntos()) {
             return 1;
         }
         return 0;
	}
	
	
}
