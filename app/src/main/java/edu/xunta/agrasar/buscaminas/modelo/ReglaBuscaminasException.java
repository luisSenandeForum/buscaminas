package edu.xunta.agrasar.buscaminas.modelo;

@SuppressWarnings("serial")
public class ReglaBuscaminasException extends Exception {
	ReglaBuscaminasException(String pMensaje) {
		super(pMensaje);
	}
}
