package edu.xunta.agrasar.buscaminas.modelo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class MisPuntuaciones {

	public final static String NIVELES[] = { "Principiante", "Intermedio", "Experto", "Personalizado" };
	private HashMap<String, ArrayList<Puntuacion>> misPnt;
	private static MisPuntuaciones INSTANCE;

	public static MisPuntuaciones getInstance() {
		if (INSTANCE == null) {
			createInstance();
		}
		return INSTANCE;
	}

	private static synchronized void createInstance() {
		INSTANCE = new MisPuntuaciones();
	}

	private MisPuntuaciones() {
		misPnt = new HashMap<>();
		misPnt.put(NIVELES[0], new ArrayList<Puntuacion>());
		misPnt.put(NIVELES[1], new ArrayList<Puntuacion>());
		misPnt.put(NIVELES[2], new ArrayList<Puntuacion>());
		misPnt.put(NIVELES[3], new ArrayList<Puntuacion>());

	}

	public void añadePuntuacion(String pNivel, Puntuacion pP) {
		ArrayList<Puntuacion> mPnt = misPnt.get(pNivel);
		if (mPnt.size() <= 7) {
			mPnt.add(pP);
			ordenarPuntuaciones();

		} else {
			Puntuacion p = mPnt.get(mPnt.size() - 1);
			if (p.compareTo(pP) > 0) {
				mPnt.add(pP);
				ordenarPuntuaciones();
				mPnt.remove(mPnt.size() - 1);
			}

		}
	}

	private void ordenarPuntuaciones() {
		for (int i = 0; i < NIVELES.length; i++) {
			Collections.sort(misPnt.get(NIVELES[i]), new Comparator<Puntuacion>() {
				public int compare(Puntuacion obj1, Puntuacion obj2) {
					return obj1.compareTo(obj2);
				}
			});
		}
	}

	public ArrayList<Puntuacion> damePuntuaciones(String pNivel){

		for (int i = 0; i < NIVELES.length; i++) {
			if(pNivel.equals(NIVELES[i])) {
				return misPnt.get(NIVELES[i]);
			}
		}
		return null;
	}
}
