package edu.xunta.agrasar.buscaminas.modelo;

import java.util.Date;

public interface Buscaminas {

	/** Inicializa el juego del buscaminas con un nivel de dificultad predeterminado */
	Buscaminas init(NivelJuego pNivel);

	/** Inicializa el juego del buscaminas con unas dimensiones dadas y con un número de minas especificado */
	Buscaminas init(int pAncho, int pAlto, int pNumMinas) throws ReglaBuscaminasException;

	/** Inicializa el juego del buscaminas con unas dimensiones dadas y con un número de minas especificado y también especificando la situación de estas minas */
	Buscaminas init(int pAncho, int pAlto, int[][] pCoordsMinas) throws ReglaBuscaminasException;

	/** Dada una coordenada del tablero, retorna si existe o no mina en dicho punto */
	boolean hayMima(int i, int j);

	/** Dada una coordenada del tablero, nos indica si esta coordenada ha sido descubierta por el jugador o no */
	boolean estaDescubierta(int i, int j);

	/** Dada un juego inicializado, se retorna si existe o no la capacidad de seguir jugando de acorde a las reglas del juego. */
	boolean terminoElJuego();

	/**
	 * Indica al juego que debe descubrirse una celda concreta especificada por unas coordenadas.
	 * 
	 * @throws ReglaBuscaminasException:
	 *             Cuando se le pase una coordenada inválida (que quede fuera del tablero)
	 */
	Buscaminas descubrir(int x, int y) throws ReglaBuscaminasException;

	/**
	 * Dada una coordenada, el jugador podrá marcar esta celda como una posible mina descubierta.
	 * 
	 * @throws ReglaBuscaminasException:
	 *             Cuando se le pase una coordenada inválida (que quede fuera del tablero)
	 */
	Buscaminas marcarMina(int x, int y) throws ReglaBuscaminasException;

	/**
	 * Dada una coordenada, el jugador podrá marcar esta celda como una celda en la que se duda si hay o no mina.
	 * 
	 * @throws ReglaBuscaminasException:
	 *             Cuando se le pase una coordenada inválida (que quede fuera del tablero)
	 */
	Buscaminas marcarDuda(int x, int y) throws ReglaBuscaminasException;

	/**
	 * Dada una coordenada, el jugador podrá desmarcar esta celda como una posible mina descubierta o una celda dudosa.
	 * 
	 * @throws ReglaBuscaminasException:
	 *             Cuando se le pase una coordenada inválida (que quede fuera del tablero)
	 */
	Buscaminas desmarcar(int x, int y) throws ReglaBuscaminasException;

	/**
	 * Dada una coordenada, se retornará el número de minas próximas a esa coordenada
	 * 
	 * @throws ReglaBuscaminasException:
	 *             Cuando se le pase una coordenada inválida (que quede fuera del tablero)
	 */
	int getNumMinasProximas(int x, int y) throws ReglaBuscaminasException;

	/** Retorna la altura en número de celdas del tablero */
	int getAltura();

	/** Retorna la anchura en número de celdas del tablero */
	int getAnchura();

	/** Retorna el número total de minas situadas en el tablero */
	int getNumTotalMinas();

	/** Retorna el número total de minas situadas en el tablero menos las que ya han sido marcadas como minas */
	int getNumMinasRestantes();

	/** Retorna el momento en el que el juego comienza */
	Date getComienzoJuego();

}
