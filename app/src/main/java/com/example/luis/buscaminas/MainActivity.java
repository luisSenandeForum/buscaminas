package com.example.luis.buscaminas;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayout;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;

import edu.xunta.agrasar.buscaminas.modelo.Buscaminas;
import edu.xunta.agrasar.buscaminas.modelo.Factoria;
import edu.xunta.agrasar.buscaminas.modelo.NivelJuego;
import edu.xunta.agrasar.buscaminas.modelo.ReglaBuscaminasException;

import static android.view.View.*;

public class MainActivity extends AppCompatActivity  implements OnClickListener{
    private
    int alto = 2;
    int ancho = 2;
    Button[][] mp_b;
    Buscaminas buscaminas;
    TextView txt_minas;
    TextView txt_segundos;
    CheckBox chk_bandera;
    Button bt_central;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        txt_minas = (TextView) findViewById(R.id.txt_minas);
        txt_segundos = (TextView) findViewById(R.id.txt_segundos);
        chk_bandera = (CheckBox) findViewById(R.id.chk_bandera);
        bt_central = (Button) findViewById(R.id.bt_central);
        buscaminas = Factoria.getInstance().crearBuscaminas().init(NivelJuego.Principiante);

        generarBotones();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.op_nuevo) {
            nuevoJuego();
        } else if(id == R.id.op_opciones) {
            return true;
        } else if(id == R.id.op_puntuaciones) {
            return true;
        } else if(id == R.id.op_salir) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void generarBotones(){
        //Obtenemos el linear layout donde colocar los botones
        TableLayout botonera = (TableLayout) findViewById(R.id.lyt_juego);

        TableLayout.LayoutParams rowParams = new TableLayout.LayoutParams(
                TableLayout.LayoutParams.FILL_PARENT, TableLayout.LayoutParams.FILL_PARENT, 1f);
        //for border
        rowParams.setMargins(2, 2, 2, 2);
        TableRow.LayoutParams itemParams = new TableRow.LayoutParams(
                TableLayout.LayoutParams.FILL_PARENT, TableLayout.LayoutParams.FILL_PARENT, 1f);


        mp_b = new Button[buscaminas.getAltura()][buscaminas.getAnchura()];
        //Creamos los botones en bucle
        for (int y=0; y < buscaminas.getAltura(); y++){
            TableRow tableRow = new TableRow(this);
            tableRow.setLayoutParams(rowParams);
            for (int x=0; x < buscaminas.getAnchura(); x++) {
                mp_b[y][x] = new Button(this);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    mp_b[y][x].setId(generateViewId());
                    mp_b[y][x].setLayoutParams(itemParams);
                }
                mp_b[y][x] .setOnClickListener(this);
                //Añadimos el botón a la botonera
                tableRow.addView(mp_b[y][x]);
            }

            botonera.addView(tableRow);
        }
    }

    @Override
    public void onClick(View v) {
        for (int y=0; y < buscaminas.getAltura(); y++){
            for (int x=0; x < buscaminas.getAnchura(); x++) {
                Button b = mp_b[y][x];
                if (v.getId() == b.getId()){
                    try {
                        System.out.println(b.isEnabled());
                        if (b.isEnabled()) {
                            //tiempo = true;
                            System.out.println(chk_bandera.isChecked());
                            if (chk_bandera.isChecked()) {
                                System.out.println("M" + b.getText().equals("M"));
                                if (b.getText().equals("M")) {
                                    buscaminas.desmarcar(y, x);
                                    b.setText(null);
                                } else {
                                    if (buscaminas.getNumMinasRestantes() > 0) {
                                        buscaminas.marcarMina(y, x);
                                        b.setText("M");
                                        b.setTextColor(Color.BLACK);
                                    }
                                }
                            } else {
                                System.out.println("M2" + b.getText());
                                if (b.getText().equals("")) {
                                    if (buscaminas.hayMima(y, x)) {
                                        finJuego(y,x);
                                        //gameOver = false;
                                        break;
                                    } else {
                                        buscaminas.descubrir(y, x);
                                        refrescar();
                                        break;
                                    }
                                }
                            }
                        }

                    } catch (ReglaBuscaminasException ex) {
                        System.out.println(ex.getMessage());
                    }
                }
            }
        }
    }

    public void nuevoJuego() {
        try {
            buscaminas = Factoria.getInstance().crearBuscaminas().init(buscaminas.getAnchura(), buscaminas.getAltura(), buscaminas.getNumTotalMinas());

                for (int x = 0; x < mp_b.length; x++) {
                    for (int y = 0; y < mp_b[x].length; y++) {
                        Button bt = mp_b[x][y];
                        bt.setEnabled(true);
                        bt.setText("");
                        bt.getBackground().clearColorFilter();
                        /*Spannable buttonLabel = new SpannableString(" Button Text");
                        buttonLabel.setSpan(new ImageSpan(getApplicationContext(), R.drawable.icon,
                                ImageSpan.ALIGN_BOTTOM), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        bt.setText(buttonLabel);
                        bt.setDisabledIcon(null);*/

                    }
                }
                txt_minas.setText(buscaminas.getNumMinasRestantes()+"");
                txt_segundos.setText(0 + "");
                /*
                bCentral.setIcon(imgCentral);
                segundos = 0;
                tiempo = false;*/

        } catch (ReglaBuscaminasException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void finJuego(int px, int py) {
        for (int x = 0; x < mp_b.length; x++) {
            for (int y = 0; y < mp_b[x].length; y++) {
                try {
                    Button bt = mp_b[x][y];
                    bt.setEnabled(false);

                    if (buscaminas.hayMima(x, y)) {
                        bt.setText("X");
                        bt.setTextColor(Color.BLACK);
                    } else if (!buscaminas.hayMima(x, y) && buscaminas.getNumMinasProximas(x, y) > 0) {
                        pintaValor(x,y,bt);
                    } else {
                        bt.setTextColor(Color.WHITE);
                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

            }
        }

        /*tiempo = false;
        if (px >= 0 || py >= 0) {
            JButton btn = campo[px][py];
            btn.setIcon(imgBoom);
            btn.setDisabledIcon(imgBoom);
            bCentral.setIcon(imgCentral_lost);

        } else {

            bCentral.setIcon(imgCentral_win);
            String dificultad = miBusca.getAltura() + "/" + miBusca.getAnchura() + " NºMinas: " + miBusca.getNumTotalMinas();
            Puntuacion pnt = new Puntuacion(segundos, dificultad);
            misPuntuaciones.añadePuntuacion(miNvl, pnt);
            puntuaciones();
        }*/
    }

    public void refrescar() {
        if (!buscaminas.terminoElJuego()) {
            for (int x = 0; x < mp_b.length; x++) {
                for (int y = 0; y < mp_b[x].length; y++) {
                    try {

                        Button bt = mp_b[x][y];
                       if (buscaminas.estaDescubierta(x, y) && !buscaminas.hayMima(x, y) && buscaminas.getNumMinasProximas(x, y) == 0) {
                            bt.setEnabled(false);
                            bt.setBackgroundColor(Color.WHITE);
                        } else if (buscaminas.estaDescubierta(x, y) && !buscaminas.hayMima(x, y) && buscaminas.getNumMinasProximas(x, y) > 0) {
                            bt.setEnabled(false);
                            pintaValor(x,y,bt);
                        }
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                }
            }

            txt_minas.setText(buscaminas.getNumMinasRestantes() + "");

        } else {
            finJuego(-1, -1);
        }
    }

    public void pintaValor(int x, int y,Button bt) throws ReglaBuscaminasException {
        switch (buscaminas.getNumMinasProximas(x, y)) {
            case 1:
                bt.setText("1");
                bt.setTextColor(Color.BLUE);
                break;
            case 2:
                bt.setText("2");
                bt.setTextColor(Color.GREEN);
                break;
            case 3:
                bt.setText("3");;
                bt.setTextColor(Color.YELLOW);
                break;
            case 4:
                bt.setText("4");
                bt.setTextColor(Color.RED);
                break;
            case 5:
                bt.setText("5");
                bt.setTextColor(Color.MAGENTA);
                break;
            case 6:
                bt.setText("6");
                bt.setTextColor(Color.MAGENTA);
                break;
            case 7:
                bt.setText("7");
                bt.setTextColor(Color.MAGENTA);
                break;
            case 8:
                bt.setText("8");
                bt.setTextColor(Color.MAGENTA);
                break;
        }
    }

}
